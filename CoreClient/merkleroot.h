#ifndef MERKLEROOT_H
#define MERKLEROOT_H

#include <string>
#include <vector>

std::string getMerkleRoot(std::vector<std::string> transactions);

#endif // MERKLEROOT_H
