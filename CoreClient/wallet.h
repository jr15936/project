#ifndef WALLET_H
#define WALLET_H

#include <unordered_map>
#include <string>
#include <cstdint>
#include <deque>

#include "transaction.h"

typedef std::unordered_map<std::string, std::unordered_map<uint32_t,TxOut>> UTXOmap;
typedef std::unordered_map<std::string,Key> keyMap;

class Wallet
{
public:

    std::string id;
    keyMap pKHtoKey;

    UTXOmap UTXOs;
    uint64_t balance;

    void updateBalance(UTXOmap &UTXOs, UTXOmap &STXOs);
    uint64_t getBalance(UTXOmap &UTXOs, UTXOmap &STXOs);
    uint64_t getPendingBalance(std::deque<Transaction> txns);

    bool createTx(std::string recipient, uint64_t value, uint8_t changeType, UTXOmap &UTXOs, UTXOmap &STXOs, Transaction &transaction);
    std::string getAddress(uint8_t type);

    Wallet() = default;
    Wallet(std::string id,UTXOmap &UTXOs, UTXOmap &STXOs);
};

#endif // WALLET_H
