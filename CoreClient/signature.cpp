#include <string>

#include "ecdsa.h"
#include "qtesla.h"
#include "key.h"
#include "signature.h"

bool verifySignature(std::string message, std::string signature, std::string pk, uint8_t type)
{
    if (type == 0) {
        return verifyECDSA(message,signature,pk);
    }
    else if (type == 1) {
        return verifyQTESLA(message,signature,pk);
    }
    return false;
}

std::string generateSignature(std::string message, std::string sk, uint8_t type)
{
    if (type == 0) {
        return generateECDSA(message,sk);
    }
    else if (type == 1) {
        return generateQTESLA(message,sk);
    }
    return "";
}

bool generateKeyPair(std::string &sk, std::string &pk, uint8_t type)
{
    if (type == 0) {
        return generateECDSAKeyPair(sk,pk);
    }
    else if (type == 1) {
        return generateQTESLAKeyPair(sk,pk);
    }
    return false;
}

Key generateKey(uint8_t type)
{
    std::string sk;
    std::string pk;
    bool result = false;
    result = generateKeyPair(sk,pk,type);
    return Key(sk,pk,type);
}
