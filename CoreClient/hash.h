#ifndef HASH_H
#define HASH_H

#include <string>

std::string hash(std::string input);

#endif // HASH_H
