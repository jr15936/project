#ifndef ECDSA_H
#define ECDSA_H

#include <string>

bool generateECDSAKeyPair(std::string &sk, std::string &pk);

std::string generateECDSA(std::string message, std::string sk);

bool verifyECDSA(std::string message, std::string signature, std::string pk);

#endif // ECDSA_H
