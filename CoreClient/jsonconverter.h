#ifndef JSONCONVERTER_H
#define JSONCONVERTER_H

#include <string>

#include "key.h"
#include "transaction.h"
#include "block.h"

std::string keyToString(Key k);

Key stringToKey(std::string k_str);

std::string toDBKey(std::string txHash, uint32_t index);

void fromDBKey(std::string s, std::string &txHash, uint32_t &index);

std::string transactionToString(Transaction transaction);

Transaction stringToTransaction(std::string s);

std::string blockToString(Block block);

Block stringToBlock(std::string s);

std::string txOutToString(TxOut txOut);

TxOut stringToTxOut(std::string s);

#endif // JSONCONVERTER_H
