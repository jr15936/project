#include "blockchain.h"
#include <string>
#include <vector>
#include <unordered_map>
#include <iostream>
#include "block.h"
#include "transaction.h"
#include "dbwrapper.h"
#include "wallet.h"
#include "transactionpool.h"
#include "merkleroot.h"

//Set mining difficulty
uint8_t Blockchain::difficulty = 4;

Blockchain::Blockchain()
{
    this->mostRecent = getMostRecent();
    //If no blocks exist in the db, create and add first block manually
    if (this->mostRecent.prevBlockHash == "") {
        TxOut utxo = TxOut(10000,"4d8a5097ab605e2a7d43e870e7f4543b78e6e592510c9d8d2a12c4b74f1765f7");
        Transaction tx = Transaction(std::vector<TxIn>(), std::vector<TxOut>({utxo}));
        this->mostRecent.txns.push_back(tx);
        ++this->mostRecent.txnCount;
        this->mostRecent.prevBlockHash = "0";
        //Add genesis UTXO
        this->UTXOs.insert({tx.calculateHash(), std::unordered_map<uint32_t,TxOut>{{0,utxo}}});
        addUTXOtoDB(utxo,tx.calculateHash(),0);
        //Mine block and add it to db
        this->mostRecent.mineBlock(this->difficulty);
        addBlockToDB(this->mostRecent);
    }
    loadUTXOs(this->UTXOs);
}

bool Blockchain::isChainValid()
{
    //ensures full chain is loaded
    this->updateChain();

    Block currBlock;
    Block prevBlock = this->chain[0];

    TxOut genesis = TxOut(prevBlock.txns[0].outputs[0]);

    std::string tHash = prevBlock.calculateHash();
    TxOut tTxOut;
    UTXOmap tUTXOs;
    tUTXOs.insert({prevBlock.txns[0].calculateHash(), std::unordered_map<uint32_t,TxOut>{{0,genesis}}});

    //Check that the first block is correct
    if (prevBlock.prevBlockHash != "0") {
        return false;
    }

    std::vector<std::string> txhs;
    for (Transaction t : prevBlock.txns) {
        txhs.push_back(t.calculateHash());
    }
    if (getMerkleRoot(txhs) != prevBlock.merkleRoot) return false;
    if (prevBlock.txnCount != prevBlock.txns.size()) return false;

    for (size_t j = 0; j < this->difficulty; ++j) {
        if (tHash[j] != '0') return false;
    }

    //Loop through blockchain checking each block
    for (size_t i = 1; i < this->chain.size(); ++i) {
        currBlock = this->chain[i];
        prevBlock = this->chain[i-1];

        tHash = currBlock.calculateHash();

        //compare hash of previous block and current blocks previous hash value;
        if (prevBlock.calculateHash() != currBlock.prevBlockHash) return false;
        //check current block has a valid hash
        for (size_t j = 0; j < this->difficulty; ++j) {
            if (tHash[j] != '0') return false;
        }

        //Loop through block checking each transaction is valid
        for (size_t j = 0; j < currBlock.txns.size(); ++j) {
            Transaction currTx = currBlock.txns[j];
            if (!currTx.verify(tUTXOs)) return false;
            for (size_t k = 0; k < currTx.inputs.size(); ++k) {
                tTxOut = tUTXOs[currTx.inputs[k].prevTXHash][currTx.inputs[k].prevTxOutIndex];
                if (tTxOut.pubKeyHash == "") return false;
                tUTXOs[currTx.inputs[k].prevTXHash].erase(currTx.inputs[k].prevTxOutIndex);
            }
            for (size_t k = 0; k < currTx.outputs.size(); ++k) {
                tUTXOs[currTx.calculateHash()].insert({k,currTx.outputs[k]});
            }
        }

        //Check merkle root of block is correct
        txhs = std::vector<std::string>();
        for (Transaction t : currBlock.txns) {
            txhs.push_back(t.calculateHash());
        }
        if (getMerkleRoot(txhs) != currBlock.merkleRoot) return false;

        //Check the block contains the correct number of transaction
        if (currBlock.txnCount != currBlock.txns.size()) return false;
        if (currBlock.txnCount > 1000) return false;

        //Check timestamps
        if (currBlock.timeStamp < prevBlock.timeStamp) return false;
    }

    return true;
}

//removes spent transaction outputs from UTXOs
void Blockchain::removeSpentTXOs(Transaction t)
{
    for (TxIn i : t.inputs) {
        this->UTXOs[i.prevTXHash].erase(i.prevTxOutIndex);
        removeUTXOfromDB(i.prevTXHash, i.prevTxOutIndex);
        if (this->UTXOs[i.prevTXHash].empty()) {
            this->UTXOs.erase(i.prevTXHash);
        }
    }
}

void Blockchain::addUTXO(TxOut UTXO, std::string hash, uint32_t index)
{
    this->UTXOs[hash].insert({index,UTXO});
}

bool Blockchain::addBlock(Block &block, TransactionPool &tp)
{
    if (block.txns.empty()) {
        return false;
    }
    block.mineBlock(this->difficulty);
    this->mostRecent = block;

    //Adds all new UTXOs from block to blockchain UTXOs and remove spent UTXOs
    std::string txhash;
    for (Transaction t : block.txns) {
        this->removeSpentTXOs(t);
        txhash = t.calculateHash();
        for (uint i = 0; i < t.outputs.size(); ++i) {
            addUTXOtoDB(t.outputs[i],txhash,i);
            this->addUTXO(t.outputs[i],txhash,i);
        }
        //Remove transaction from transaction pool
        tp.removeTransaction(t);
    }
    this->chain.push_back(block);
    if (!addBlockToDB(block)) return false;
    return true;
}

//Constructs the entire blockchain from the database
void Blockchain::updateChain()
{
    Block temp;
    if (this->chain.empty()) {
        temp = getMostRecent();
        this->chain.push_front(temp);
    }
    else {
        temp = this->chain.front();
    }
    while (temp.prevBlockHash != "0") {
        temp = getBlock(temp.prevBlockHash);
        this->chain.push_front(temp);
    }
}
