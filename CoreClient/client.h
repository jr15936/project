#ifndef CLIENT_H
#define CLIENT_H

#include <string>
#include <cstdint>

#include <QFrame>
#include <QTextEdit>
#include <QPushButton>
#include <QComboBox>
#include <QString>
#include <QWidget>
#include <QDialog>
#include <QLineEdit>
#include <QLabel>

#include "blockchain.h"
#include "transactionpool.h"
#include "wallet.h"
#include "block.h"

class Client : public QFrame
{
    Q_OBJECT

public:

    Blockchain *blockchain;
    TransactionPool *tp;
    Wallet *wallet;
    int currAddType;

    Transaction currTx;
    Block currBlock;

    //Block mostRecentBlock;

    QTextEdit *blockText;
    QTextEdit *txText;
    QTextEdit *txPoolText;

    QTextEdit *balance;
    QTextEdit *address;


    QPushButton *loadWalletButton;
    QPushButton *getBalanceButton;
    QPushButton *sendFundsButton;
    QPushButton *addTxToBlockButton;
    QPushButton *addBlockToChainButton;
    QPushButton *findBlockButton;
    QPushButton *viewChainButton;
    QPushButton *viewUTXOsButton;
    QPushButton *viewTpButton;
    QPushButton *findTxButton;
    QPushButton *recentBlockButton;
    QComboBox *addTypeBox;

    Block  findBlock;
    QLabel *findBlockLabel;
    QLineEdit *findBlockText;

    QString tempadd;
    QString tempval;
    std::string teststring;

    void updateTpText();
    void updateBlockText(Block block);

    explicit Client(QWidget *parent = nullptr);

public slots:
    void handleGetBalance();
    void handleGetAddress();

private slots:
    void handleLoadWallet();
    void handleSendFunds();
    void handleAddTxToBlock();
    void handleAddBlockToChain();
    void handleFindBlock();
    void handleRecentBlock();
    void handleViewChain();
    void handleAddType(int idx);
    void handleViewUTXOs();
    void handleViewTp();

};

class ChainDialog : public QDialog
{
    Q_OBJECT
public:

    explicit ChainDialog(Client *parent = nullptr);

private:
    Client *p;
    QTextEdit *chainText;
    QPushButton *isValidButton;
    QPushButton *close;

private slots:
    void handleIsValid();
};

class WalletDialog : public QDialog
{
    Q_OBJECT
public:
    Client *p;

    explicit WalletDialog(Client *parent = nullptr);

private:
    QLabel *idlabel;
    QLineEdit *idline;
    QPushButton *newButton;
    QPushButton *loadButton;
    QPushButton *cancelButton;

private slots:
    void handleNew();
    void handleLoad();
};

class TxDialog : public QDialog
{
    Q_OBJECT
public:

    Client *p;
    uint8_t cT;

    explicit TxDialog(Client *parent = nullptr);

private:
    QLabel *addlabel;
    QLabel *vallabel;
    QLineEdit *addline;
    QLineEdit *valline;
    QPushButton *createButton;
    QComboBox *changeTypeBox;
    QPushButton *cancel;

private slots:
    void handleCreate();
    void handleChange(int);

};

class ErrorDialog : public QDialog
{
    Q_OBJECT
public:
    explicit ErrorDialog(Client *parent = nullptr, std::string e = "");

private:
    QLabel *errortext;
    QPushButton *closeButton;
};

class SuccessDialog : public ErrorDialog
{
    Q_OBJECT
public:
    explicit SuccessDialog(Client *parent = nullptr, std::string s = "");
};

#endif // CLIENT_H
