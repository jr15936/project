#include "test.h"
#include "qtesla.h"
#include "ecdsa.h"
#include "signature.h"
#include "blockchain.h"
#include "block.h"
#include "hash.h"
#include "key.h"
#include "transactionpool.h"
#include "transaction.h"
#include "jsonconverter.h"

#include <iostream>
#include <string>
#include <vector>

typedef std::unordered_map<std::string, std::unordered_map<uint32_t,TxOut>> UTXOmap;
typedef std::unordered_map<std::string,Key> keyMap;

namespace test {

//signature tests
//qTESLA tests
bool qTESLAtest()
{
    std::string sk,pk,signature;
    std::vector<std::string> messages = {
        "", "abc", "def", "0",
        "\n\0", "0x01", "hello",
        "195DF318FF28EEB8EC109ED6DA27F817B604DB81E4CC723563B9FAE8BFC64E52",
        "213FEA215E216E712B8873423C021312FE56AEFFC82372331203213210EABC0123B120CB2309C2382888810231BC378C23BC8237C2B3C283723BC8CB8BC2387B82382B832B8732904290BB0CWQB",
        "HJKDJSA;L'ALs';aLSAsA;SLA'LS;lAS"
    };
    for (size_t i = 0; i < 10; ++i) {
        for (size_t j = 0; j < 10; ++j) {
            generateQTESLAKeyPair(sk,pk);
            signature = generateQTESLA(messages[j],sk);
            if (!verifyQTESLA(messages[j],signature,pk)) return false;
        }

    }
    return true;
}

bool ECDSAtest()
{
    std::string sk,pk,signature;
    std::vector<std::string> messages = {
        "", "abc", "def", "0",
        "\n\0", "0x01", "hello",
        "195DF318FF28EEB8EC109ED6DA27F817B604DB81E4CC723563B9FAE8BFC64E52",
        "213FEA215E216E712B8873423C021312FE56AEFFC82372331203213210EABC0123B120CB2309C2382888810231BC378C23BC8237C2B3C283723BC8CB8BC2387B82382B832B8732904290BB0CWQB",
        "HJKDJSA;L'ALs';aLSAsA;SLA'LS;lAS"
    };
    for (size_t i = 0; i < 10; ++i) {
        for (size_t j = 0; j < 10; ++j) {
            generateECDSAKeyPair(sk,pk);
            signature = generateECDSA(messages[j],sk);
            if (!verifyECDSA(messages[j],signature,pk)) return false;
        }

    }
    return true;
}

bool signatureTest()
{
    std::string signature;
    Key k;
    std::vector<std::string> messages = {
        "", "abc", "def", "0",
        "\n\0", "0x01", "hello",
        "195DF318FF28EEB8EC109ED6DA27F817B604DB81E4CC723563B9FAE8BFC64E52",
        "213FEA215E216E712B8873423C021312FE56AEFFC82372331203213210EABC0123B120CB2309C2382888810231BC378C23BC8237C2B3C283723BC8CB8BC2387B82382B832B8732904290BB0CWQB",
        "HJKDJSA;L'ALs';aLSAsA;SLA'LS;lAS"
    };
    for (size_t j = 0; j < 10; ++j) {
        k = generateKey(0);
        signature = generateSignature(messages[j],k.privateKey,k.type);
        if (!verifySignature(messages[j],signature,k.publicKey,k.type)) return false;
    }
    for (size_t j = 0; j < 10; ++j) {
        k = generateKey(1);
        signature = generateSignature(messages[j],k.privateKey,k.type);
        if (!verifySignature(messages[j],signature,k.publicKey,k.type)) return false;
    }
    return true;
}

//Block tests

//Transaction tests
bool transactionTest()
{
    UTXOmap UTXOs;
    TxOut to1 (100,"e819128c6fff3c0169f7117605df671d2a07d7f763b85589328e3b5b03cf8adc");
    TxOut to2 (200,"5865316e4bc413c06a304ea4e6aadd749fd3b69f16a2b19f0d70c66456bfca89");
    TxOut to3 (300,"bc7dd46aad8e633d478d6c1d66a9ff08e0ce8b34bf4f12b6620ccc5c7f42b447");
    TxOut to4 (400,"ff7b07ff8bd70edebfacda133bc126d3a78f4875598a64452be23c54e4483185");
    TxOut to5 (500,"0cf40c06f40bf7ec69b870c41616853477b818c989a2e3a9de818fe3b489e422");
    TxOut to6 (600,"389820a2c360ce8d510acafb470ecd0d4c30708876e447f1f57485922bec5c50");
    TxOut to7 (700,"34310a54a7c65e643a351cf6b1f93fe48dea4bce36759719b4b26ffd3dc92544");
    TxOut to8 (800,"88207291336a40fa0041add1597b805872684043de86cf8491d715d194333f73");

    UTXOs["a"].insert({0,to1});
    UTXOs["a"].insert({1,to2});
    UTXOs["a"].insert({2,to3});
    UTXOs["b"].insert({0,to4});
    UTXOs["b"].insert({1,to5});
    UTXOs["b"].insert({2,to6});
    UTXOs["c"].insert({0,to7});
    UTXOs["c"].insert({1,to8});

    TxIn ti1 ("a",0,0,"","02845ac746e223537d72e06b05254bab290febdba46d7c2a84d5929e8378fbcca8");
    TxIn ti2 ("a",0,0,"","0216d8c7d40b0c7dc9bf58336fa15e2b1c1f753ba1ed7a6267c6345d1805f56c6f");
    TxIn ti3 ("b",0,0,"","037ab79c75f1c3a3039a5275555583a5080d589df8e108f05ab1c7482b956071b1");
    TxIn ti4 ("c",1,0,"","03655f736dc0902ca56df92a040afa250dc0555f647e3867157e5aa7ddf0764912");

    TxOut u1 (100,"test1");
    TxOut u2 (500,"test2");
    TxOut u3 (500,"test3");
    TxOut u4 (200,"test4");

    Transaction tx0;
    Transaction tx1 (std::vector<TxIn> {ti1}, std::vector<TxOut> {u1});
    Transaction tx2 (std::vector<TxIn> {ti2}, std::vector<TxOut> {u1});
    Transaction tx3 (std::vector<TxIn> {ti3,ti4}, std::vector<TxOut> {u2,u3,u4});

    tx0.generateSignatures(std::vector<std::string>(),std::vector<std::string>());
    tx1.generateSignatures(std::vector<std::string>{"e819128c6fff3c0169f7117605df671d2a07d7f763b85589328e3b5b03cf8adc"},std::vector<std::string>{"6459dcee19b0782f5a7c7e3dc73f07fa107f02ff727951b63d5e998c1c1e6f12"});
    tx2.generateSignatures(std::vector<std::string>{"e819128c6fff3c0169f7117605df671d2a07d7f763b85589328e3b5b03cf8adc"},std::vector<std::string>{"6459dcee19b0782f5a7c7e3dc73f07fa107f02ff727951b63d5e998c1c1e6f12"});
    tx3.generateSignatures(std::vector<std::string>{"ff7b07ff8bd70edebfacda133bc126d3a78f4875598a64452be23c54e4483185","88207291336a40fa0041add1597b805872684043de86cf8491d715d194333f73"},std::vector<std::string>{"6a62a0d545c0d3d127f72fffe8ecf728cf228c31ae7418a8adef28869074ccd6","08e9e6623a0891a411f87254356d7422914b91169ec19479ae628384909b6f31"});
    if (tx0.verify(UTXOs)) return false;
    if (!tx1.verify(UTXOs)) return false;
    if (tx2.verify(UTXOs)) return false;
    if (!tx3.verify(UTXOs)) return false;

    return true;
}


bool jsonTest()
{
    TxOut to1 (100,"e819128c6fff3c0169f7117605df671d2a07d7f763b85589328e3b5b03cf8adc");
    TxOut to2 (200,"5865316e4bc413c06a304ea4e6aadd749fd3b69f16a2b19f0d70c66456bfca89");
    TxIn ti1 ("a",0,0,"","02845ac746e223537d72e06b05254bab290febdba46d7c2a84d5929e8378fbcca8");
    TxIn ti2 ("a",0,0,"","0216d8c7d40b0c7dc9bf58336fa15e2b1c1f753ba1ed7a6267c6345d1805f56c6f");
    Transaction tx1 (std::vector<TxIn> ({ti1,ti2}), std::vector<TxOut> ({to1,to2}));
    Transaction tx2 (std::vector<TxIn> ({ti2}), std::vector<TxOut> ({to1}));
    Block block = Block("0000d9733955f0021d4523c0576c0a4ff22b4c84e481e33b152f9719b8937057");
    block.txns.push_back(tx1);
    block.txns.push_back(tx2);
    block.mineBlock(4);

    Key k = generateKey(0);

    TxOut txout;
    std::string temp;
    temp = transactionToString(tx1);
    if (stringToTransaction(temp) != tx1) return false;
    temp = transactionToString(tx2);
    if (stringToTransaction(temp) != tx2) return false;
    temp = txOutToString(to1);
    txout = stringToTxOut(temp);
    if (txout.pubKeyHash != to1.pubKeyHash || txout.value != to1.value) return false;
    temp = txOutToString(to2);
    txout = stringToTxOut(temp);
    if (txout.pubKeyHash != to2.pubKeyHash || txout.value != to2.value) return false;
    if (stringToBlock(blockToString(block)).calculateHash() != block.calculateHash()) return false;
    Key c = stringToKey(keyToString(k));
    if (c.privateKey != k.privateKey || c.publicKey != k.publicKey || c.type != k.type) return false;
    return true;
}

bool blockTests()
{
    TxOut to1 (100,"e819128c6fff3c0169f7117605df671d2a07d7f763b85589328e3b5b03cf8adc");
    TxOut to2 (200,"5865316e4bc413c06a304ea4e6aadd749fd3b69f16a2b19f0d70c66456bfca89");
    TxIn ti1 ("a",0,0,"","02845ac746e223537d72e06b05254bab290febdba46d7c2a84d5929e8378fbcca8");
    TxIn ti2 ("a",0,0,"","0216d8c7d40b0c7dc9bf58336fa15e2b1c1f753ba1ed7a6267c6345d1805f56c6f");
    Transaction tx1 (std::vector<TxIn> ({ti1,ti2}), std::vector<TxOut> ({to1,to2}));
    Transaction tx2 (std::vector<TxIn> ({ti2}), std::vector<TxOut> ({to1}));
    Block block("0000d9733955f0021d4523c0576c0a4ff22b4c84e481e33b152f9719b8937057");
    block.txns.push_back(tx1);
    block.txns.push_back(tx2);
    block.mineBlock(4);
    for (size_t i =0; i < 4; ++i) {
        if (block.calculateHash()[i] != '0') return false;
    }
    return true;
}

void all_tests()
{
    uint success = 0;
    uint failure = 0;
    (qTESLAtest()) ? ++success : ++failure;
    (ECDSAtest()) ? ++success : ++failure;
    (signatureTest()) ? ++success : ++failure;
    (transactionTest()) ? ++success : ++failure;
    (jsonTest()) ? ++success : ++failure;
    (blockTests()) ? ++success : ++failure;

    std::cout << success << " tests passed." << std::endl;
    std::cout << failure << " tests failed." << std::endl;
    return;
}

} //END TEST NAMESPACE
