#include <unordered_map>
#include <string>
#include <cstdint>
#include <deque>
#include <algorithm>

#include "transactionpool.h"
#include "transaction.h"
#include "block.h"
#include "dbwrapper.h"

typedef std::unordered_map<std::string, std::unordered_map<uint32_t,TxOut>> UTXOmap;

//Adds a transaction to the pool
bool TransactionPool::addTransaction(Transaction transaction, UTXOmap &UTXOs)
{
    if (!(transaction.verify(UTXOs))) return false;
    this->pool.push_back(transaction);
    for (TxIn t : transaction.inputs) {
        this->STXOs[t.prevTXHash].insert({t.prevTxOutIndex,UTXOs[t.prevTXHash][t.prevTxOutIndex]});
    }

    return true;
}

//removes a transaction from the pool
bool TransactionPool::removeTransaction(Transaction transaction)
{
    auto it = this->pool.begin();
    for (it = this->pool.begin(); it != this->pool.end(); ++ it) {
        if (it->getRawTx() == transaction.getRawTx()) break;
    }
    if (it == this->pool.end()) return false;
    this->pool.erase(it);
    this->STXOs.erase(transaction.calculateHash());
    return true;
}

//Adds transactions from the pool to the block
bool TransactionPool::addTxToBlock(Block &block, UTXOmap UTXOs)
{
    size_t i = block.txns.size();
    if (this->pool.empty()) return false;
    if (block.txns.size() >= 1000) return false;

    for (auto it = this->pool.begin(); it != this->pool.end(); ++it) {
        if (block.txns.size() >= 1000) break;
        if (std::find(block.txns.begin(), block.txns.end(), *it) == block.txns.end()) {
            block.addTransaction(*it,UTXOs);
        }
    }
    if (i == block.txns.size()) {
        return false;
    }
    return true;
}
