#include <iostream>
#include <string>
#include <vector>

#include <QApplication>
#include <QLabel>

#include "test.h"

#include "client.h"

int main(int argc, char *argv[])
{
    test::all_tests();

    QApplication app(argc,argv);

    Client *client = new Client();
    client->setFixedSize(1200,1000);

    client->show();

    return app.exec();
}
