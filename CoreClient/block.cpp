#include <string>
#include <vector>

#include "block.h"
#include "transaction.h"
#include "merkleroot.h"
#include "hash.h"
#include "gettime.h"

void Block::mineBlock(uint8_t difficulty)
{
    this->setMerkleRoot();
    std::string target (difficulty, '0');

    std::string hash = this->calculateHash();
    //Increase nonce and recompute hash till first 'difficulty' characters are '0'
    while (std::string(hash.begin(), hash.begin() + difficulty) != target) {
        ++nonce;
        hash = this->calculateHash();
    }
}

void Block::setMerkleRoot()
{
    std::vector<std::string> transactions;
    for (Transaction t : this->txns) {
        transactions.push_back(t.calculateHash());
    }
    this->merkleRoot = getMerkleRoot(transactions);
}

std::string Block::calculateHash()
{
    return hash(
        this->prevBlockHash +
        this->merkleRoot +
        std::to_string(this->txnCount) +
        std::to_string(this->timeStamp) +
        std::to_string(this->nonce)
    );
}

bool Block::addTransaction(Transaction transaction, UTXOmap &UTXOs)
{
    if (this->txnCount >= 1000) return false;
    if (!(transaction.verify(UTXOs))) return false;
    this->txns.push_back(transaction);
    ++this->txnCount;
    return true;
}

Block::Block(std::string prevHash)
{
    this->prevBlockHash = prevHash;
    this->timeStamp = getTime();
    this->nonce = 0;
    this->txnCount = 0;
    this->txns = std::vector<Transaction>();
}
