#ifndef QTESLA_H
#define QTESLA_H

#include <string>

bool generateQTESLAKeyPair(std::string &sk, std::string &pk);

std::string generateQTESLA(std::string message, std::string sk);

bool verifyQTESLA(std::string message, std::string signature, std::string pk);

#endif // QTESLA_H
