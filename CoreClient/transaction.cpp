#include <string>
#include <vector>
#include <cstdint>

#include "transaction.h"
#include "hash.h"
#include "signature.h"

//Returns TxOut, TxIn, Tx as a basic string
//Use when signing transactions
std::string TxOut::getRawTxOut()
{
    std::string out = std::to_string(this->value) + pubKeyHash;
    return out;
}

std::string TxIn::getRawTxIn()
{
    std::string out = this->prevTXHash +
        std::to_string(this->prevTxOutIndex) +
        std::to_string(this->sigType) +
        this->signature +
        this->pubKey;
    return out;
}

std::string Transaction::getRawTx()
{
    std::string output = "";
    for (TxIn t : this->inputs) {
        output += t.getRawTxIn();
    }
    for (TxOut t : this->outputs) {
        output += t.getRawTxOut();
    }
    return output;
}

std::string Transaction::calculateHash()
{
    std::string out;
    for (TxIn t : this->inputs) {
        out += t.getRawTxIn();
    }
    for (TxOut t : this->outputs) {
        out += t.getRawTxOut();
    }
    return hash(out);
}

//Calculates total value of inputs in transaction
uint64_t Transaction::getInputsValue(UTXOmap &UTXOs)
{
    uint64_t v = 0;
    for (TxIn t : this->inputs) {
        v += UTXOs[t.prevTXHash][t.prevTxOutIndex].value;
    }
    return v;
}

//Calculates total value of outputs in transaction
uint64_t Transaction::getOutputsValue()
{
    uint64_t v = 0;
    for (TxOut t : this->outputs) {
        v += t.value;
    }
    return v;
}

//Returns true if TxOut is addressed to a Key in the map
bool TxOut::isMine(std::unordered_map<std::string,Key> pubKeyHashes)
{
    return (pubKeyHashes.find(this->pubKeyHash) != pubKeyHashes.end());
}

//generates signature of this transaction
std::string Transaction::sign(uint8_t sigType, std::string sk)
{
    return generateSignature(this->getRawTx(),sk,sigType);
}

//generates signatures for all the transaction inputs in this transaction
//prevPubKeyHashes is a list of the TxOut addresses for which the TxIns in this transaction reference
//Keys is a list of private keys, key[i] = sk, sk->pk, hash256(pk)=prevPubKeyHashes[i]
bool Transaction::generateSignatures(std::vector<std::string> prevPubkeyHashes, std::vector<std::string> keys)
{
    if (prevPubkeyHashes.size() != keys.size()) return false;
    if (keys.size() != this->inputs.size()) return false;
    std::vector<std::string> sigs;
    Transaction tempTx = *this;
    for (size_t i = 0; i < this->inputs.size(); ++i) {
        tempTx.inputs[i].signature = "";
    }
    Transaction t = tempTx;
    for (size_t i = 0; i < this->inputs.size(); ++i) {
        t.inputs[i].signature = prevPubkeyHashes[i];
        sigs.push_back(t.sign(t.inputs[i].sigType, keys[i]));
        t = tempTx;
    }
    for (size_t i = 0; i < sigs.size(); ++i) {
        this->inputs[i].signature = sigs[i];
    }
    return true;
}

//Returns true if all the signatures in this transactions inputs are valid
bool Transaction::verifySignatures(std::vector<std::string> prevPubKeyHashes)
{
    Transaction tempTx = Transaction(*this);
    std::vector<std::string> pubKeys;
    for (size_t i = 0; i < tempTx.inputs.size(); ++i) {
        pubKeys.push_back(tempTx.inputs[i].pubKey);
        tempTx.inputs[i].signature = "";
    }
    for (size_t i = 0; i < pubKeys.size(); ++i) {
        Transaction t = tempTx;
        t.inputs[i].signature = prevPubKeyHashes[i];
        if (!verifySignature(t.getRawTx(),this->inputs[i].signature,t.inputs[i].pubKey,t.inputs[i].sigType)) {
            return false;
        }
    }
    return true;
}

//Checks that a transaction is valid
bool Transaction::verify(UTXOmap &UTXOs)
{
    if (this->inputs.empty() || this->outputs.empty()) return false;
    //Check that all UTXOs spent in transaction are contained in the UTXOmap
    std::vector<std::string> pubKeyHashes;
    std::string prevPubKeyHash;
    for (TxIn t : this->inputs) {
        prevPubKeyHash = UTXOs[t.prevTXHash][t.prevTxOutIndex].pubKeyHash;
        pubKeyHashes.push_back(prevPubKeyHash);
        if (prevPubKeyHash != hash(t.pubKey)) return false;
    }
    //Check the signatures are valid
    if (!this->verifySignatures(pubKeyHashes)) return false;

    //Check input amount == output amount
   if (this->getInputsValue(UTXOs) != this->getOutputsValue()) return false;

    return true;
}

bool operator==(const Transaction &tx1, const Transaction &tx2)
{
    return (Transaction(tx1).calculateHash() == Transaction(tx2).calculateHash());
}

bool operator!=(const Transaction &tx1, const Transaction &tx2)
{
    return !(tx1 == tx2);
}
