#ifndef BLOCKCHAIN_H
#define BLOCKCHAIN_H

#include <unordered_map>
#include <string>
#include <deque>
#include <cstdint>

#include "transaction.h"
#include "block.h"
#include "transactionpool.h"

typedef std::unordered_map<std::string, std::unordered_map<uint32_t,TxOut>> UTXOmap;

class Blockchain
{
public:
    UTXOmap UTXOs;
    static uint8_t difficulty;
    Block mostRecent;
    std::deque<Block> chain;

    bool isChainValid();
    bool addBlock(Block &block, TransactionPool &tp);
    void addUTXO(TxOut UTXO, std::string hash, uint32_t index);
    void removeSpentTXOs(Transaction t);
    void updateChain();

    Blockchain();
};

#endif // BLOCKCHAIN_H
