#include <chrono>
#include <cstdint>
#include "gettime.h"

uint32_t getTime()
{
    auto now = std::chrono::system_clock::now();
    auto now_ms = std::chrono::time_point_cast<std::chrono::milliseconds>(now);
    auto value = now_ms.time_since_epoch();
    return uint32_t(value.count());
}
