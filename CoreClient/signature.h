#ifndef SIGNATURE_H
#define SIGNATURE_H

#include <string>
#include <cstdint>

#include "key.h"

/* This header handles the calls to different signature scheme algorithms
 * uint8_t type decides which algorithm should be called
 * type = 0: Calls ECDSA algorithms
 * type = 1: Calls qTeslaI algorithms
 */

bool verifySignature(std::string message, std::string signature, std::string pk, uint8_t type);

std::string generateSignature(std::string message, std::string sk, uint8_t type);

Key generateKey(uint8_t type);

#endif // SIGNATURE_H
