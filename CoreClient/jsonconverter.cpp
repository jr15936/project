#include "jsonconverter.h"
#include <vector>
#include <string>
#include <iostream>
#include <cstdint>
#include <jsoncpp/json/json.h>
#include "transaction.h"
#include "block.h"
#include "key.h"

//Constructs a JSON object from a string
Json::Value stringToJson(std::string s)
{
    Json::Reader reader;
    Json::Value obj;
    reader.parse(s,obj);
    return obj;
}

//converts keys to and from json strings
std::string keyToString(Key k)
{
    Json::Value obj;
    obj["privateKey"] = k.privateKey;
    obj["publicKey"] = k.publicKey;
    obj["type"] = k.type;
    return obj.toStyledString();
}

Key stringToKey(std::string k_str)
{
    Key k;
    Json::Value obj = stringToJson(k_str);
    k.privateKey = obj["privateKey"].asString();
    k.publicKey = obj["publicKey"].asString();
    k.type = uint8_t(obj["type"].asUInt());
    return k;
}

//converts a hash and an index into/from a UTXO database key
std::string toDBKey(std::string txHash, uint32_t index)
{
    Json::Value obj;
    obj["txHash"] = txHash;
    obj["index"] = index;
    return obj.toStyledString();
}

void fromDBKey(std::string s, std::string &txHash, uint32_t &index)
{
    Json::Value obj = stringToJson(s);
    txHash = obj["txHash"].asString();
    index = obj["index"].asUInt();
}

TxOut jsonToTxOut(Json::Value obj)
{
    uint64_t value = std::stoull(obj["value"].asString());
    std::string pubKeyHash = obj["pubKeyHash"].asString();
    return TxOut(value,pubKeyHash);
}

Json::Value txOutToJson(TxOut txOut)
{
    Json::Value jTxOut;
    jTxOut["value"] = std::to_string(txOut.value);
    jTxOut["pubKeyHash"] = txOut.pubKeyHash;
    return jTxOut;
}

Transaction jsonToTransaction(Json::Value obj)
{
    std::string prevTXHash;
    uint32_t prevTxOutIndex;
    uint8_t sigType;
    std::string signature;
    std::string pubKey;

    uint64_t value;
    std::string pubKeyHash;

    std::vector<TxIn> inputs;
    std::vector<TxOut> outputs;

    const Json::Value &jins = obj["inputs"];
    const Json::Value &jouts = obj["outputs"];

    for (uint i = 0; i < jins.size(); ++i) {
        prevTXHash = jins[i]["prevTXHash"].asString();
        prevTxOutIndex = jins[i]["prevTxOutIndex"].asUInt();
        sigType = uint8_t(jins[i]["sigType"].asUInt());
        signature = jins[i]["signature"].asString();
        pubKey = jins[i]["pubKey"].asString();

        inputs.push_back(TxIn(prevTXHash, prevTxOutIndex, sigType, signature, pubKey));
    }

    for (uint i = 0; i < jouts.size(); ++i) {
        value = std::stoull(jouts[i]["value"].asString());
        pubKeyHash = jouts[i]["pubKeyHash"].asString();

        outputs.push_back(TxOut(value,pubKeyHash));
    }

    return Transaction(inputs, outputs);
}

Json::Value transactionToJson(Transaction transaction)
{
    Json::Value jTransaction;
    Json::Value jins;
    Json::Value jouts;

    TxIn tempIn;

    for (uint i = 0; i < transaction.inputs.size(); ++i) {
        jins[i]["prevTXHash"] = transaction.inputs[i].prevTXHash;
        jins[i]["prevTxOutIndex"] = transaction.inputs[i].prevTxOutIndex;
        jins[i]["sigType"] = transaction.inputs[i].sigType;
        jins[i]["signature"] = transaction.inputs[i].signature;
        jins[i]["pubKey"] = transaction.inputs[i].pubKey;
    }

    for (uint i = 0; i < transaction.outputs.size(); ++i) {
        jouts[i]["value"] = std::to_string(transaction.outputs[i].value);
        jouts[i]["pubKeyHash"] = transaction.outputs[i].pubKeyHash;
    }

    jTransaction["inputs"] = jins;
    jTransaction["outputs"] = jouts;

    return jTransaction;
}

Block jsonToBlock(Json::Value obj)
{
    std::string prevBlockHash;
    std::string merkleRoot;
    uint32_t timeStamp;
    uint32_t nonce;
    uint16_t txnCount;
    std::vector<Transaction> txns;

    prevBlockHash = obj["prevBlockHash"].asString();
    merkleRoot = obj["merkleRoot"].asString();
    timeStamp = obj["timeStamp"].asUInt();
    nonce = obj["nonce"].asUInt();
    txnCount = uint16_t(obj["txnCount"].asUInt());
    const Json::Value &jtxns = obj["txns"];

    for (uint i = 0; i < jtxns.size(); ++i) {
        txns.push_back(jsonToTransaction(jtxns[i]));
    }

    return Block(prevBlockHash,merkleRoot,timeStamp,nonce,txnCount,txns);
}

Json::Value blockToJson(Block block)
{
    Json::Value jBlock;
    Json::Value jTxns;
    Json::Value temp;

    for (uint i = 0; i < block.txns.size(); ++i) {
        temp = transactionToJson(block.txns[i]);
        jTxns[i] = temp;
    }

    jBlock["prevBlockHash"] = block.prevBlockHash;
    jBlock["merkleRoot"] = block.merkleRoot;
    jBlock["timeStamp"] = block.timeStamp;
    jBlock["nonce"] = block.nonce;
    jBlock["txnCount"] = block.txnCount;
    jBlock["txns"] = jTxns;

    return jBlock;
}

std::string transactionToString(Transaction transaction)
{
    return transactionToJson(transaction).toStyledString();
}

Transaction stringToTransaction(std::string s)
{
    return jsonToTransaction(stringToJson(s));
}

std::string blockToString(Block block)
{
    return blockToJson(block).toStyledString();
}

Block stringToBlock(std::string s)
{
    return jsonToBlock(stringToJson(s));
}

std::string txOutToString(TxOut txOut)
{
    return txOutToJson(txOut).toStyledString();
}

TxOut stringToTxOut(std::string s)
{
    return jsonToTxOut(stringToJson(s));
}
