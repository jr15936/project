#ifndef TRANSACTION_H
#define TRANSACTION_H

#include <cstdint>
#include <string>
#include <unordered_map>
#include <vector>

#include <key.h>

class TxOut
{
public:
    uint64_t value;
    std::string pubKeyHash;

    bool isMine(std::unordered_map<std::string,Key> pubKeyHashes);
    std::string getRawTxOut();

    TxOut() = default;
    TxOut(uint64_t v, std::string pKH) : value(v), pubKeyHash(pKH) {}
};

class TxIn
{
public:
    std::string prevTXHash;
    uint32_t prevTxOutIndex;
    uint8_t sigType;
    std::string signature;
    std::string pubKey;

    std::string getRawTxIn();

    TxIn() = default;
    TxIn(std::string p, uint32_t pTOI, std::string pk) :
        prevTXHash(p), prevTxOutIndex(pTOI), pubKey(pk) {}
    TxIn(std::string p, uint32_t pTOI, uint8_t sigT, std::string sig, std::string pk) :
        prevTXHash(p), prevTxOutIndex(pTOI), sigType(sigT), signature(sig), pubKey(pk) {}
};

typedef std::unordered_map<std::string, std::unordered_map<uint32_t,TxOut>> UTXOmap;

class Transaction
{
public:

    std::vector<TxIn> inputs;
    std::vector<TxOut> outputs;

    std::string calculateHash();
    uint64_t getInputsValue(UTXOmap &UTXOs);
    uint64_t getOutputsValue();
    bool verify(UTXOmap &UTXOs);
    std::string sign(uint8_t sigType, std::string sk);
    bool generateSignatures(std::vector<std::string> prevPubkeyHashes, std::vector<std::string> keys);
    bool verifySignatures(std::vector<std::string> prevPubkeyHashes);
    std::string getRawTx();

    bool operator==(Transaction &tx1) { return this->calculateHash() == tx1.calculateHash(); }

    Transaction() = default;
    Transaction(std::vector<TxIn> i, std::vector<TxOut> o) : inputs(i), outputs(o) {}
};

bool operator==(const Transaction &tx1, const Transaction &tx2);

bool operator!=(const Transaction &tx1, const Transaction &tx2);


#endif // TRANSACTION_H
