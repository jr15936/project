#ifndef BLOCK_H
#define BLOCK_H

#include <unordered_map>
#include <string>
#include <cstdint>
#include <vector>

#include "transaction.h"

typedef std::unordered_map<std::string, std::unordered_map<uint32_t,TxOut>> UTXOmap;

class Block
{
public:
    std::string prevBlockHash;
    std::string merkleRoot;
    uint32_t timeStamp;
    uint32_t nonce;
    uint16_t txnCount;
    std::vector<Transaction> txns;

    std::string calculateHash();
    void setMerkleRoot();
    void mineBlock(uint8_t difficulty);
    bool addTransaction(Transaction transaction, UTXOmap &UTXOs);

    Block() = default;
    Block(std::string prevHash);
    Block(std::string p, std::string m, uint32_t t, uint32_t n, uint16_t c,std::vector<Transaction> s) :
        prevBlockHash(p), merkleRoot(m), timeStamp(t), nonce(n), txnCount(c), txns(s) {}
};

#endif // BLOCK_H
