\contentsline {chapter}{\numberline {1}Contextual background}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Quantum Computing}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Cryptocurrency}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Summary}{4}{section.1.3}
\contentsline {chapter}{\numberline {2}Cryptocurrency Design}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Hash Functions}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Proof of Work}{6}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Alternatives to Proof-of-Work}{7}{subsection.2.2.1}
\contentsline {section}{\numberline {2.3}Consensus}{8}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}51\% Attacks}{9}{subsection.2.3.1}
\contentsline {section}{\numberline {2.4}Transactions}{10}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Transaction Inputs}{10}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Transaction Outputs}{11}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Scripts}{11}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Pay-to-Pubkey-Hash Transactions}{11}{subsection.2.4.4}
\contentsline {subsection}{\numberline {2.4.5}Other Transaction Types}{11}{subsection.2.4.5}
\contentsline {subsubsection}{Pay-to-Pubkey Transactions}{11}{section*.5}
\contentsline {subsubsection}{Pay-to-Script-Hash}{12}{section*.6}
\contentsline {subsection}{\numberline {2.4.6}Signatures}{12}{subsection.2.4.6}
\contentsline {section}{\numberline {2.5}Merkle Trees}{13}{section.2.5}
\contentsline {chapter}{\numberline {3}Effects of Quantum Computing on Cryptocurrencies}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}Shor's Algorithm}{15}{section.3.1}
\contentsline {section}{\numberline {3.2}Grover's Algorithm}{16}{section.3.2}
\contentsline {section}{\numberline {3.3}Effects on Bitcoin}{16}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Shor's Algorithm}{16}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Grover's Algorithm}{18}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Effects on other Cryptocurrencies}{18}{section.3.4}
\contentsline {chapter}{\numberline {4}Post Quantum Cryptography}{19}{chapter.4}
\contentsline {section}{\numberline {4.1}Hash-based}{19}{section.4.1}
\contentsline {section}{\numberline {4.2}Lattice-based}{22}{section.4.2}
\contentsline {section}{\numberline {4.3}Code-based}{25}{section.4.3}
\contentsline {section}{\numberline {4.4}Multivariate-based}{26}{section.4.4}
\contentsline {section}{\numberline {4.5}Supersingular Isogeny-based}{27}{section.4.5}
\contentsline {section}{\numberline {4.6}Comparison}{27}{section.4.6}
\contentsline {chapter}{\numberline {5}Blockchain design and implementation}{29}{chapter.5}
\contentsline {section}{\numberline {5.1}Blocks and Transactions}{30}{section.5.1}
\contentsline {section}{\numberline {5.2}Signatures}{32}{section.5.2}
\contentsline {section}{\numberline {5.3}Transaction Pool}{35}{section.5.3}
\contentsline {section}{\numberline {5.4}Hash and Merkle root}{35}{section.5.4}
\contentsline {section}{\numberline {5.5}JSON and Database Storage}{36}{section.5.5}
\contentsline {section}{\numberline {5.6}Wallet}{37}{section.5.6}
\contentsline {section}{\numberline {5.7}User Interface}{37}{section.5.7}
\contentsline {chapter}{\numberline {6}Evaluation}{41}{chapter.6}
\contentsline {section}{\numberline {6.1}Unit tests}{41}{section.6.1}
\contentsline {section}{\numberline {6.2}User Interface testing}{41}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Wallet}{41}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Transactions}{42}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Blockchain}{45}{subsection.6.2.3}
\contentsline {subsection}{\numberline {6.2.4}Database}{46}{subsection.6.2.4}
\contentsline {section}{\numberline {6.3}Evaluation of technology choices}{46}{section.6.3}
\contentsline {section}{\numberline {6.4}Limitations}{47}{section.6.4}
\contentsline {section}{\numberline {6.5}Known issues}{48}{section.6.5}
\contentsline {section}{\numberline {6.6}Comparison to alternatives}{48}{section.6.6}
\contentsline {section}{\numberline {6.7}Incorporation of this design in current cryptocurrencies}{50}{section.6.7}
\contentsline {chapter}{\numberline {7}Conclusion}{51}{chapter.7}
