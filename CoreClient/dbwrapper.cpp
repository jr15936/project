#include <unordered_map>
#include <string>
#include <cstdint>

#include <leveldb/db.h>
#include <boost/filesystem.hpp>

#include "dbwrapper.h"
#include "transaction.h"
#include "block.h"
#include "key.h"
#include "hash.h"
#include "jsonconverter.h"

bool createDirectory(std::string path)
{
    boost::filesystem::path dir(path);
    if (boost::filesystem::create_directory(dir)) return true;
    return false;
}

void createDirectories()
{
    createDirectory("data");
    createDirectory("data/wallets");
    createDirectory("data/blocks");
    createDirectory("data/utxos");
}

bool directoryExists(std::string path)
{
    boost::filesystem::path dir(path);
    return (boost::filesystem::is_directory(dir));
}

bool addKeyToDB(std::string id, Key k)
{
    leveldb::DB *db;
    leveldb::Options options;
    options.create_if_missing = true;
    leveldb::Status status = leveldb::DB::Open(options,"data/wallets/"+id,&db);

    if (!status.ok()) return false;

    db->Put(leveldb::WriteOptions(),hash(k.publicKey),keyToString(k));

    delete db;

    return true;
}

bool loadWallet(std::string id, keyMap &pKHtoKey)
{
    leveldb::DB *db;
    leveldb::Options options;
    options.create_if_missing = true;
    leveldb::Status status = leveldb::DB::Open(options,"data/wallets/"+id,&db);

    if (!status.ok()) return false;

    Key k;
    leveldb::Iterator *it = db->NewIterator(leveldb::ReadOptions());
    for (it->SeekToFirst(); it->Valid(); it->Next()) {
        pKHtoKey.insert({it->key().ToString(),stringToKey(it->value().ToString())});
    }

    if (!it->status().ok()) return false;

    delete it;
    delete db;

    return true;
}

Block getMostRecent()
{
    std::string hash;
    std::string blockString;
    leveldb::DB *db;
    leveldb::Options options;
    options.create_if_missing = true;
    leveldb::Status status = leveldb::DB::Open(options,"data/blocks",&db);
    db->Get(leveldb::ReadOptions(),"mostRecent",&hash);
    db->Get(leveldb::ReadOptions(),hash,&blockString);
    delete db;
    return stringToBlock(blockString);
}

bool loadUTXOs(UTXOmap &UTXOs)
{
    leveldb::DB *db;
    leveldb::Options options;
    options.create_if_missing = true;
    std::string path = "data/utxos";

    leveldb::Status status = leveldb::DB::Open(options,path,&db);

    if (!status.ok()) {
        return false;
    }

    std::string key;
    std::string utxoString;

    TxOut UTXO;
    std::string txHash;
    uint32_t index;

    leveldb::Iterator *it = db->NewIterator(leveldb::ReadOptions());
    for (it->SeekToFirst(); it->Valid(); it->Next()) {
        key = it->key().ToString();
        utxoString = it->value().ToString();

        UTXO = stringToTxOut(utxoString);

        fromDBKey(key,txHash,index);
        UTXOs[txHash].insert({index,UTXO});
    }

    if (!it->status().ok()) {
        return false;
    }

    delete it;
    delete db;

    return true;
}

bool addUTXOtoDB(TxOut UTXO,std::string txHash, uint32_t index)
{
    leveldb::DB* db;
    leveldb::Options options;
    options.create_if_missing = true;
    std::string path = "data/utxos";


    leveldb::Status status = leveldb::DB::Open(options, path, &db);

    std::string key = toDBKey(txHash,index);

    db->Put(leveldb::WriteOptions(),key,txOutToString(UTXO));
    delete db;

    return true;

}

bool removeUTXOfromDB(std::string txHash, uint32_t index)
{
    leveldb::DB* db;
    leveldb::Options options;
    options.create_if_missing = true;
    std::string path = "data/utxos";


    leveldb::Status status = leveldb::DB::Open(options, path, &db);

    std::string key = toDBKey(txHash,index);

    db->Delete(leveldb::WriteOptions(),key);
    delete db;
    return true;
}

bool addBlockToDB(Block block)
{
    leveldb::DB* db;
    leveldb::Options options;
    options.create_if_missing = true;

    leveldb::Status status = leveldb::DB::Open(options, "data/blocks", &db);

    if (!status.ok()) {
        return false;
    }

    db->Put(leveldb::WriteOptions(),block.calculateHash(),blockToString(block));
    db->Put(leveldb::WriteOptions(),"mostRecent",block.calculateHash());
    delete db;

    return true;
}

Block getBlock(std::string hash)
{
    std::string blockString;
    leveldb::DB *db;
    leveldb::DB::Open(leveldb::Options(),"data/blocks",&db);
    db->Get(leveldb::ReadOptions(),hash,&blockString);
    Block block = stringToBlock(blockString);
    delete db;
    return block;
}
