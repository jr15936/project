#include "transaction.h"
#include "hash.h"
#include "key.h"
#include "signature.h"
#include "dbwrapper.h"
#include <string>
#include <vector>
#include <deque>
#include <cstdint>
#include <iostream>
#include <unordered_map>
#include "wallet.h"

typedef std::unordered_map<std::string, std::unordered_map<uint32_t,TxOut>> UTXOmap;
typedef std::unordered_map<std::string,Key> keyMap;

Wallet::Wallet(std::string id,UTXOmap &UTXOs, UTXOmap &STXOs)
{
    this->balance = 0;
    this->id = id;
    loadWallet(id,this->pKHtoKey);
    updateBalance(UTXOs, STXOs);
}

void Wallet::updateBalance(UTXOmap &UTXOs, UTXOmap &STXOs)
{
    //add transactions from blockchain utxos to wallet utxos
    for (auto pt = UTXOs.begin(); pt != UTXOs.end(); ++pt) {
        for (auto qt = pt->second.begin(); qt != pt->second.end(); ++qt) {
            if (qt->second.isMine(this->pKHtoKey) && STXOs[pt->first].find(qt->first) == STXOs[pt->first].end()) {
                if (this->UTXOs.find(pt->first) == this->UTXOs.end()) {
                    this->UTXOs.insert({pt->first,std::unordered_map<uint32_t,TxOut>()});
                }
                if (this->UTXOs[pt->first].find(qt->first) == this->UTXOs[pt->first].end()) {
                    this->UTXOs[pt->first].insert({qt->first,qt->second});
                    balance += qt->second.value;
                }
            }
        }
    }
}

uint64_t Wallet::getBalance(UTXOmap &UTXOs, UTXOmap &STXOs)
{
    updateBalance(UTXOs, STXOs);
    return this->balance;
}

//Gets the balance of UTXOs addressed to this wallet which are in the transaction pool
uint64_t Wallet::getPendingBalance(std::deque<Transaction> txns)
{
    uint64_t pending = 0;
    for (Transaction tx : txns) {
        for (TxOut t : tx.outputs) {
            if (t.isMine(this->pKHtoKey)) pending += t.value;
        }
    }
    return pending;
}

bool Wallet::createTx(std::string recipient, uint64_t value, uint8_t changeType, UTXOmap &UTXOs, UTXOmap &STXOs, Transaction &transaction)
{
    if (value == 0) return false;
    if (recipient == "") return false;
    //Check the wallet has sufficient funds
    if (this->getBalance(UTXOs, STXOs) < value) return false;

    std::vector<TxIn> inputs;
    std::vector<TxOut> outputs;

    uint64_t total = 0;
    std::vector<std::string> prevPubKeyHashes;
    std::vector<std::string> privateKeys;
    TxOut t;
    Key k;

    //Gets the UTXOs to create the transaction and removes them from the wallets UTXO list
    for (auto pt = this->UTXOs.begin(); pt != this->UTXOs.end(); ) {
        for (auto qt = pt->second.begin(); qt != pt->second.end(); ) {
            t = qt->second;
            k = this->pKHtoKey[t.pubKeyHash];
            if (STXOs[pt->first].find(qt->first) == STXOs[pt->first].end()) {
                prevPubKeyHashes.push_back((t.pubKeyHash));
                privateKeys.push_back(k.privateKey);
                total += t.value;
                this->balance -= t.value;
                inputs.push_back(TxIn(pt->first, qt->first, k.type, "", k.publicKey));
                qt = pt->second.erase(qt);
                if (total >= value) break;
            }
        }
        if (pt->second.empty()) {
            pt = this->UTXOs.erase(pt);
        }
        if (total >= value) break;
    }

    uint64_t change = total - value;

    //Creates the outputs: one to recipient and change sent back to this wallet
    outputs.push_back(TxOut(value,recipient));
    std::string changeAddress = this->getAddress(changeType);
    if (change > 0) outputs.push_back(TxOut(change,changeAddress));

    transaction = Transaction(inputs,outputs);
    transaction.generateSignatures(prevPubKeyHashes,privateKeys);

    return true;
}

/*Gets an address of a given type
 *type = 0: ECDSA address
 *type = 1: qTESLA_I address
 */
std::string Wallet::getAddress(uint8_t type)
{
    for (std::pair<std::string,Key> p : this->pKHtoKey) {
        if (p.second.type == type) {
            return p.first;
        }
    }
    Key k = generateKey(type);
    std::string pKH = hash(k.publicKey);
    this->pKHtoKey.insert({pKH,k});
    if (!addKeyToDB(this->id,k)) return "";
    return pKH;
}
