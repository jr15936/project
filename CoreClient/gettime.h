#ifndef GETTIME_H
#define GETTIME_H

#include <cstdint>

uint32_t getTime();

#endif // GETTIME_H
