#include <oqs_cpp.h>

#include <sstream>
#include <string>

std::string byteToString(oqs::bytes input)
{
    //Convert byte vector to hex string
    std::stringstream ss;
    for (oqs::byte b : input) {
        ss << std::setfill('0') << std::setw(2) << std::hex << int(b);
    }
    return ss.str();
}

oqs::bytes stringToBytes(std::string input)
{
    //Convert hex string to byte vector
    oqs::bytes output;
    for (auto pt = input.begin(); pt != input.end(); pt+=2) {
        std::string temp = std::string(pt,pt+2);
        uint8_t t = uint8_t(std::stoi(temp,nullptr,16));
        output.push_back(t);
    }
    return output;
}

oqs::bytes messageToBytes(std::string input)
{
    //Convert message to byte vector
    oqs::bytes output;
    uint8_t t;
    for (char c : input) {
        t = uint8_t(c);
        output.push_back(t);
    }
    return output;
}

bool generateQTESLAKeyPair(std::string &sk, std::string &pk)
{
    oqs::Signature s {"qTESLA_III_SIZE"};
    pk = byteToString(s.generate_keypair());
    sk = byteToString(s.export_secret_key());
    return true;
}

std::string generateQTESLA(std::string message, std::string sk)
{
    oqs::bytes skb = stringToBytes(sk);
    oqs::Signature signer {"qTESLA_III_SIZE",skb};
    oqs::bytes sb = signer.sign(messageToBytes(message));
    return byteToString(sb);
}

bool verifyQTESLA(std::string message, std::string signature, std::string pk)
{
    oqs::Signature verifier {"qTESLA_III_SIZE"};
    return verifier.verify(messageToBytes(message),stringToBytes(signature),stringToBytes(pk));
}
