#ifndef DBWRAPPER_H
#define DBWRAPPER_H

#include <unordered_map>
#include <string>
#include <cstdint>
#include <vector>

#include "transaction.h"
#include "key.h"
#include "block.h"

typedef std::unordered_map<std::string, std::unordered_map<uint32_t,TxOut>> UTXOmap;
typedef std::unordered_map<std::string,Key> keyMap;

void createDirectories();

bool directoryExists(std::string path);

bool addKeyToDB(std::string id, Key k);

bool loadWallet(std::string id, keyMap  &pKHtoKey);

Block getMostRecent();

bool loadUTXOs(UTXOmap &UTXOs);

bool removeUTXOfromDB(std::string txHash, uint32_t index);

bool addUTXOtoDB(TxOut UTXO,std::string txHash, uint32_t index);

bool addBlockToDB(Block block);

Block getBlock(std::string hash);

#endif // DBWRAPPER_H
