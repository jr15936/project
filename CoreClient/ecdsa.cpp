#include <string>

#include <crypto++/osrng.h>
#include <crypto++/hex.h>
#include <crypto++/filters.h>
#include <crypto++/eccrypto.h>
#include <cryptopp/oids.h>

#include "ecdsa.h"

bool generateECDSAKeyPair(std::string &sk, std::string &pk)
{
    using namespace CryptoPP;

    //Initialise random generator. and use to generate a private key
    AutoSeededRandomPool prng;
    ECDSA<ECP,SHA256>::PrivateKey privateKey;
    ECDSA<ECP,SHA256>::PublicKey publicKey;
    privateKey.Initialize( prng, ASN1::secp256k1() );
    bool result = privateKey.Validate( prng, 3 );
    if( !result ) return false;
    //Use private key to create public key
    privateKey.MakePublicKey(publicKey);
    result = publicKey.Validate( prng, 3 );
    if (!result) return false;

    //Use point compression to shorten public key length
    publicKey.AccessGroupParameters().SetPointCompression(true);

    std::string p,pp;
    publicKey.Save(StringSink(pp).Ref());
    StringSource uu (pp,true,new HexEncoder(new StringSink(p)));

    std::string l,ll;
    privateKey.Save(StringSink(ll).Ref());
    StringSource vv (ll,true,new HexEncoder(new StringSink(l)));

    //Get just the public/private key from the key data and convert to lowercase
    std::string temp = std::string(p.end()-66,p.end());
    pk = "";
    for (char c : temp) { pk += std::tolower(c); }
    temp = std::string(l.end()-64,l.end());
    sk = "";
    for (char c : temp) { sk += std::tolower(c); }

    return true;
}

std::string generateECDSA(std::string message, std::string sk)
{
    using namespace CryptoPP;

    //Convert the private key string to the cryptoPP key type
    HexDecoder decoder;
    decoder.Put((byte*)&sk[0],sk.size());
    decoder.MessageEnd();

    Integer x;
    x.Decode(decoder,decoder.MaxRetrievable());

    ECDSA<ECP,SHA256>::PrivateKey privateKey;
    privateKey.Initialize(ASN1::secp256k1(),x);

    //Sign the message
    ECDSA<ECP,SHA256>::Signer signer(privateKey);

    AutoSeededRandomPool prng;
    std::string signature;
    StringSource s(message,true,new SignerFilter(prng, signer, new HexEncoder(new StringSink(signature))));

    //Convert output to lowercase string
    std::string output = "";
    for (char c : signature) {
        output += std::tolower(c);
    }
    return output;
}

bool verifyECDSA(std::string message, std::string signature, std::string pk)
{
    using namespace CryptoPP;

    //Construct public key from string
    ECDSA<ECP,SHA256>::PublicKey pubKey;
    pubKey.AccessGroupParameters().Initialize(ASN1::secp256k1());

    std::string compactPoint = pk;

    StringSource ss (compactPoint,true,new HexDecoder);
    ECP::Point point;

    pubKey.GetGroupParameters().GetCurve().DecodePoint(point,ss,ss.MaxRetrievable());
    pubKey.SetPublicElement(point);

    ECDSA<ECP,SHA256>::Verifier verifier(pubKey);

    bool result = false;

    //Convert hex signature to byte string and then check if valid
    std::string sigbin;
    StringSource tt (signature,true,new HexDecoder(new StringSink(sigbin)));
    StringSource uu (sigbin+message,true,new SignatureVerificationFilter(verifier,new ArraySink((byte*)&result,sizeof(result))));

    return result;
}
