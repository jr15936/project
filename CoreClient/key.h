#ifndef KEY_H
#define KEY_H

#include <string>
#include <cstdint>

class Key
{
public:
    std::string privateKey;
    std::string publicKey;
    uint8_t type;

    Key() = default;
    Key(std::string sk, std::string pk, uint8_t t) : privateKey(sk), publicKey(pk), type(t) {}
};

#endif // KEY_H
