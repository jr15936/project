#ifndef TRANSACTIONPOOL_H
#define TRANSACTIONPOOL_H

#include <unordered_map>
#include <string>
#include <deque>

#include "transaction.h"
#include "block.h"

typedef std::unordered_map<std::string, std::unordered_map<uint32_t,TxOut>> UTXOmap;

class TransactionPool
{
public:
    std::deque<Transaction> pool;
    UTXOmap STXOs;

    bool addTransaction(Transaction transaction, UTXOmap &UTXOs);
    bool addTxToBlock(Block &block, UTXOmap UTXOs);
    bool removeTransaction(Transaction transaction);

    TransactionPool() = default;
};

#endif // TRANSACTIONPOOL_H
