#include <string>
#include <vector>
#include <iostream>

#include "merkleroot.h"
#include "hash.h"

std::string getMerkleRoot(std::vector<std::string> transactions)
{
    //base recursion cases
    if (transactions.empty()) return hash("");
    if (transactions.size() == 1) {
        return hash(transactions[0]);
    }
    if (transactions.size() == 2) {
        return hash(transactions[0] + transactions[1]);
    }
    //Calculate next level of tree and call function recursively on new layer
    std::vector<std::string> hashes;
    for (size_t i = 0; i < transactions.size() - 1; i+=2) {
        hashes.push_back(hash(transactions[i] + transactions[i+1]));
    }
    if (transactions.size() % 2 != 0) {
        hashes.push_back(transactions[transactions.size()-2]);
    }
    return getMerkleRoot(hashes);
}
