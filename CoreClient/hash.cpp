#include <string>
#include <cctype>

#include <crypto++/sha3.h>
#include <crypto++/hex.h>
#include <crypto++/filters.h>

#include "hash.h"

std::string hash(std::string input)
{
    byte digest[CryptoPP::SHA3_256::DIGESTSIZE];
    byte digest2[CryptoPP::SHA3_256::DIGESTSIZE];
    CryptoPP::SHA3_256 hash;
    //Compute SHA3_256 of input
    hash.CalculateDigest(digest,(const byte*)input.c_str(), input.length());
    //Compute SHA3_256 of bytes of SHA3_256(input)
    hash.CalculateDigest(digest2,digest,CryptoPP::SHA3_256::DIGESTSIZE);

    //Encode bytes into hex string;
    CryptoPP::HexEncoder encoder;
    std::string temp;
    encoder.Attach(new CryptoPP::StringSink(temp));
    encoder.Put(digest2,sizeof(digest2));
    encoder.MessageEnd();

    //convert into lowercase hex string
    std::string output = "";
    for (char c : temp) {
        output += std::tolower(c);
    }
    return output;
}
