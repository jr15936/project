#include <string>
#include <cstdint>

#include "client.h"
#include "transaction.h"
#include "jsonconverter.h"
#include "qtesla.h"
#include "dbwrapper.h"

#include <QApplication>
#include <QLabel>
#include <QDialog>
#include <QDialogButtonBox>
#include <QFormLayout>
#include <QLineEdit>
#include <QComboBox>

Client::Client(QWidget *parent) : QFrame(parent)
{
    //created directorys for data if neeeded
    createDirectories();

    //Blockchain objects
    this->blockchain = new Blockchain();
    this->tp = new TransactionPool();
    this->currAddType = 0;

    this->currTx = Transaction();
    this->currBlock = Block(this->blockchain->mostRecent.calculateHash());

    //Buttons
    this->loadWalletButton = new QPushButton(this);
    this->loadWalletButton->setGeometry(800,900,120,30);
    this->loadWalletButton->setText("Load Wallet");

    this->getBalanceButton = new QPushButton("Update Balance",this);
    this->getBalanceButton->setGeometry(670,900,120,30);
    this->getBalanceButton->setEnabled(false);

    this->sendFundsButton = new QPushButton(this);
    this->sendFundsButton->setGeometry(800,940,120,30);
    this->sendFundsButton->setText("Send Funds");
    this->sendFundsButton->setEnabled(false);

    this->addTxToBlockButton = new QPushButton(this);
    this->addTxToBlockButton->setGeometry(365,30,220,30);
    this->addTxToBlockButton->setText("Add Transactions To Block");

    this->addBlockToChainButton = new QPushButton(this);
    this->addBlockToChainButton->setGeometry(950,900,220,30);
    this->addBlockToChainButton->setText("Add Block To Chain");

    this->addTypeBox = new QComboBox(this);
    this->addTypeBox->setGeometry(670,940,120,30);
    this->addTypeBox->addItem("ECDSA");
    this->addTypeBox->addItem("qTESLA");
    this->addTypeBox->setEnabled(false);

    this->viewChainButton = new QPushButton(this);
    this->viewChainButton->setGeometry(950,940,220,30);
    this->viewChainButton->setText("View Blockchain");

    this->viewUTXOsButton = new QPushButton(this);
    this->viewUTXOsButton->setGeometry(210,30,145,30);
    this->viewUTXOsButton->setText("View UTXOs");

    this->viewTpButton = new QPushButton(this);
    this->viewTpButton->setGeometry(30,30,170,30);
    this->viewTpButton->setText("View Transaction Pool");

    this->findBlockButton = new QPushButton(this);
    this->findBlockButton->setText("Find");
    this->findBlockButton->setGeometry(1020,30,70,30);

    this->recentBlockButton = new QPushButton(this);
    this->recentBlockButton->setGeometry(1100,30,70,30);
    this->recentBlockButton->setText("Recent");


    //Labels
    this->findBlockLabel = new QLabel(this);
    this->findBlockLabel->setText("Block Hash: ");
    this->findBlockLabel->setGeometry(615,30,90,30);

    //textedits
    this->blockText = new QTextEdit(this);
    this->blockText->setStyleSheet("QTextEdit {font-family : Consolas;}");
    this->blockText->setGeometry(615,70,555,800);
    this->blockText->setReadOnly(true);
    this->blockText->clear();
    this->blockText->moveCursor(QTextCursor::Start);
    this->blockText->append(QString::fromStdString(blockToString(this->blockchain->mostRecent)));

    this->txPoolText = new QTextEdit(this);
    this->txPoolText->setStyleSheet("QTextEdit {font-family : Consolas;}");
    this->txPoolText->setGeometry(30,70,555,800);
    this->txPoolText->setReadOnly(true);
    this->updateTpText();

    this->balance = new QTextEdit(this);
    this->balance->setGeometry(30,900,630,30);
    this->balance->setText("Balance: ");
    this->balance->setReadOnly(true);

    this->address = new QTextEdit(this);
    this->address->setGeometry(30,940,630,30);
    this->address->setText("Address: ");
    this->address->setReadOnly(true);

    //Lineedits
    this->findBlockText = new QLineEdit(this);
    this->findBlockText->setGeometry(705,30,305,30);

    //Connect buttons to slots
    QObject::connect(loadWalletButton, SIGNAL (released()), this, SLOT (handleLoadWallet()));
    QObject::connect(getBalanceButton, SIGNAL( released()), this, SLOT (handleGetBalance()));
    QObject::connect(sendFundsButton, SIGNAL (released()), this, SLOT (handleSendFunds()));
    QObject::connect(addTxToBlockButton, SIGNAL (released()), this, SLOT (handleAddTxToBlock()));
    QObject::connect(addBlockToChainButton, SIGNAL (released()), this, SLOT (handleAddBlockToChain()));
    QObject::connect(findBlockButton, SIGNAL (released()), this, SLOT (handleFindBlock()));
    QObject::connect(recentBlockButton, SIGNAL (released()), this, SLOT (handleRecentBlock()));
    QObject::connect(addTypeBox, SIGNAL (activated(int)), this, SLOT (handleAddType(int)));
    QObject::connect(viewChainButton, SIGNAL (released()), this, SLOT (handleViewChain()));
    QObject::connect(viewTpButton, SIGNAL (released()), this, SLOT (handleViewTp()));
    QObject::connect(viewUTXOsButton, SIGNAL (released()), this, SLOT (handleViewUTXOs()));
}

ChainDialog::ChainDialog(Client *parent) : QDialog(parent)
{
    this->p = parent;
    this->chainText = new QTextEdit();
    this->chainText->setReadOnly(true);
    this->close = new QPushButton("Close");
    this->isValidButton = new QPushButton("Verify Blockchain");

    p->blockchain->updateChain();

    this->chainText->clear();
    this->chainText->moveCursor(QTextCursor::Start);
    for (Block b : p->blockchain->chain) {
        this->chainText->append(QString::fromStdString(blockToString(b)));
    }

    QObject::connect(close, SIGNAL (released()), this, SLOT (close()));
    QObject::connect(isValidButton, SIGNAL (released()), this, SLOT (handleIsValid()));

    QHBoxLayout *l1 = new QHBoxLayout();
    l1->addWidget(this->isValidButton);
    l1->addWidget(this->close);
    QVBoxLayout *lM = new QVBoxLayout();
    lM->addWidget(chainText);
    lM->addLayout(l1);
    this->setLayout(lM);
    this->setWindowTitle("Blockchain");
    this->setMinimumSize(QSize(600,800));
}

WalletDialog::WalletDialog(Client *parent) : QDialog (parent)
{
    this->p = parent;

    this->idlabel = new QLabel("Wallet ID: ");
    this->idline = new QLineEdit();

    this->newButton = new QPushButton("Create");
    this->loadButton = new QPushButton("Load");
    this->cancelButton = new QPushButton("Cancel");

    QObject::connect(newButton, SIGNAL (released()), this, SLOT (handleNew()));
    QObject::connect(loadButton, SIGNAL (released()), this, SLOT (handleLoad()));
    QObject::connect(cancelButton, SIGNAL (released()), this, SLOT (close()));

    QHBoxLayout *top = new QHBoxLayout();
    QHBoxLayout *bot = new QHBoxLayout();
    QVBoxLayout *main = new QVBoxLayout();
    top->addWidget(this->idlabel);
    top->addWidget(this->idline);
    bot->addWidget(this->loadButton);
    bot->addWidget(this->newButton);
    bot->addWidget(this->cancelButton);
    main->addLayout(top);
    main->addLayout(bot);
    setLayout(main);
    this->setWindowTitle("Load Wallet");
    this->setFixedHeight(sizeHint().height());
}

TxDialog::TxDialog(Client *parent) : QDialog(parent)
{
    this->p = parent;
    this->cT = 0;

    this->addlabel = new QLabel("To: ",this);
    this->vallabel = new QLabel("Amount: ",this);

    this->addline = new QLineEdit(this);
    this->valline = new QLineEdit(this);

    this->createButton = new QPushButton("Create",this);
    this->createButton->setDefault(true);

    this->cancel = new QPushButton("Cancel",this);

    this->changeTypeBox = new QComboBox();
    this->changeTypeBox->addItem("ECDSA");
    this->changeTypeBox->addItem("qTesla");

    QObject::connect(createButton, SIGNAL (released()), this, SLOT (handleCreate()));
    QObject::connect(cancel, SIGNAL (released()), this, SLOT (close()));
    QObject::connect(changeTypeBox, SIGNAL (activated(int)), this, SLOT (handleChange(int)));

    QHBoxLayout *l1 = new QHBoxLayout();
    l1->addWidget(this->addlabel);
    l1->addWidget(this->addline);

    QHBoxLayout *l2 = new QHBoxLayout();
    l2->addWidget(this->vallabel);
    l2->addWidget(this->valline);

    QHBoxLayout *l3 = new QHBoxLayout();
    l3->addWidget(this->createButton);
    l3->addWidget(this->changeTypeBox);
    l3->addWidget(this->cancel);

    QVBoxLayout *lM = new QVBoxLayout();
    lM->addLayout(l1);
    lM->addLayout(l2);
    lM->addLayout(l3);
    lM->addStretch();
    this->setLayout(lM);


    this->setWindowTitle("Send Funds");
    this->setMinimumWidth(600);

}

ErrorDialog::ErrorDialog(Client *parent, std::string e) : QDialog (parent)
{
    this->errortext = new QLabel(QString::fromStdString(e));
    this->closeButton = new QPushButton("Close");

    QObject::connect(closeButton, SIGNAL (released()), this, SLOT (close()));

    QHBoxLayout *mainLayout = new QHBoxLayout();
    mainLayout->addWidget(errortext);
    mainLayout->addWidget(closeButton);

    this->setLayout(mainLayout);
    this->setWindowTitle("Error Message");
    this->setFixedHeight(sizeHint().height());

}

SuccessDialog::SuccessDialog(Client *parent, std::string s) : ErrorDialog (parent,s)
{
    this->setWindowTitle("Success");
}

void TxDialog::handleChange(int idx)
{
    this->cT = uint8_t(idx);
}

void Client::handleAddType(int idx)
{
    this->currAddType = idx;
    this->handleGetAddress();
}

void WalletDialog::handleLoad()
{
    std::string id = this->idline->text().toStdString();
    if (id == "") {
        ErrorDialog *e = new ErrorDialog(p,"Wallet id cannot be empty!");
        e->show();
        return;
    }
    std::string path = "data/wallets/" +id;
    if (!directoryExists(path)) {
        ErrorDialog *e = new ErrorDialog(p,"Wallet with this id does not exist!");
        e->show();
        return;
    }
    this->p->wallet = new Wallet(id,p->blockchain->UTXOs,p->tp->STXOs);
    this->p->handleGetAddress();
    this->p->handleGetBalance();
    this->p->sendFundsButton->setEnabled(true);
    this->p->getBalanceButton->setEnabled(true);
    this->p->addTypeBox->setEnabled(true);
    this->close();
}

void WalletDialog::handleNew()
{
    std::string id = this->idline->text().toStdString();
    if (id == "") {
        ErrorDialog *e = new ErrorDialog(p,"Wallet id cannot be empty!");
        e->show();
        return;
    }
    std::string path = "data/wallets/" +id;
    if (directoryExists(path)) {
        ErrorDialog *e = new ErrorDialog(p,"Wallet with this id already exists!");
        e->show();
        return;
    }
    this->p->wallet = new Wallet(id,p->blockchain->UTXOs,p->tp->STXOs);
    this->p->handleGetAddress();
    this->p->handleGetBalance();
    this->p->sendFundsButton->setEnabled(true);
    this->p->getBalanceButton->setEnabled(true);
    this->p->addTypeBox->setEnabled(true);
    this->close();
}

void TxDialog::handleCreate()
{
    QString valtext = valline->text();
    std::string address = addline->text().toStdString();
    uint64_t value = valline->text().toULongLong();
    Transaction tx;
    if (!p->wallet->createTx(address,value,this->cT,p->blockchain->UTXOs,p->tp->STXOs,tx)) {
        ErrorDialog *e = new ErrorDialog(p,"Failed to create transaction!");
        e->show();
        this->close();
        return;
    }

    if (tx.verify(p->blockchain->UTXOs)) {
        p->currTx = tx;
        p->tp->addTransaction(tx,p->blockchain->UTXOs);
        //addTxToTxPoolDB(tx);
        //p->blockchain->removeSpentTXOs(tx);
        p->updateTpText();
        SuccessDialog *s = new SuccessDialog(p,"Transaction created and added to Transaction Pool!");
        s->show();
        p->handleGetBalance();
    }
    else {
        ErrorDialog *e = new ErrorDialog(p,"Verification error!");
        e->show();
    }
    this->close();
}

void Client::updateTpText()
{
    std::string tpstr = "";
    for (Transaction t : tp->pool) {
        tpstr += transactionToString(t);
    }
    if (tpstr == "") {
        tpstr = "Transaction Pool is Empty!";
    }
    this->txPoolText->clear();
    this->txPoolText->moveCursor(QTextCursor::Start);
    this->txPoolText->append(QString::fromStdString(tpstr));
}

void Client::updateBlockText(Block block)
{
    this->blockText->clear();
    this->blockText->moveCursor(QTextCursor::Start);
    this->blockText->append(QString::fromStdString(blockToString(block)));
    this->findBlockText->clear();
    this->findBlockText->setText(QString::fromStdString(block.calculateHash()));
}

void Client::handleGetBalance()
{
    QString s = QString("Balance: ");
    s += QString::fromStdString(std::to_string(this->wallet->getBalance(this->blockchain->UTXOs,this->tp->STXOs)));
    uint64_t pending = this->wallet->getPendingBalance(this->tp->pool);
    if (pending > 0) s += QString::fromStdString(" (" + std::to_string(pending) + " pending)");
    this->balance->setText(s);
    for (auto p : this->blockchain->UTXOs) {
        for (auto q : p.second) {
        }
    }
}

void Client::handleGetAddress()
{
    QString s = QString("Address: ");
    s += QString::fromStdString(this->wallet->getAddress(uint8_t(this->currAddType)));
    this->address->setText(s);
}

void Client::handleSendFunds()
{
    TxDialog *txDialog = new TxDialog(this);
    txDialog->show();
}

void Client::handleAddTxToBlock()
{
    if (!this->tp->pool.empty()) {
        if (this->tp->addTxToBlock(this->currBlock,this->blockchain->UTXOs)) {
            this->updateBlockText(this->currBlock);
            SuccessDialog *s = new SuccessDialog(this,"Transactions added to Block!");
            s->show();
        }
        else {
            ErrorDialog *e = new ErrorDialog(this,"Error adding transactions to Block!");
            e->show();
        }
    }
    else {
        ErrorDialog *e = new ErrorDialog(this,"Transaction Pool is Empty!");
        e->show();
    }
}

void Client::handleAddBlockToChain()
{
    if (this->blockchain->addBlock(this->currBlock,*(this->tp))) {
        this->updateBlockText(this->currBlock);
        this->currBlock = Block(this->currBlock.calculateHash());
        this->updateTpText();
        SuccessDialog *s = new SuccessDialog(this,"Block mined and added to chain!");
        s->show();
        if (getBalanceButton->isEnabled()) {
            this->handleGetBalance();
        }
    }
    else {
        ErrorDialog *e = new ErrorDialog(this,"Problem adding block to chain!");
        e->show();
    }
}

void Client::handleFindBlock()
{
    this->findBlock = getBlock(this->findBlockText->text().toStdString());
    if (this->findBlock.prevBlockHash == "") {
        ErrorDialog *e = new ErrorDialog(this,"Block does not exist!");
        e->show();
        return;
    }
    else {
        this->updateBlockText(this->findBlock);
    }
}

void Client::handleRecentBlock()
{
    this->updateBlockText(this->blockchain->mostRecent);
}

void Client::handleLoadWallet()
{
    WalletDialog *wDialog = new WalletDialog(this);
    wDialog->show();
}

void Client::handleViewChain()
{
    ChainDialog *cDialog = new ChainDialog(this);
    cDialog->show();
}

void Client::handleViewUTXOs()
{
    std::string s = "";
    for (auto p : this->blockchain->UTXOs) {
        for (auto q : p.second) {
            s += txOutToString(q.second);
        }
    }
    this->txPoolText->clear();
    this->txPoolText->moveCursor(QTextCursor::Start);
    this->txPoolText->append(QString::fromStdString(s));
}

void Client::handleViewTp()
{
    updateTpText();
}

void ChainDialog::handleIsValid()
{
    if (this->p->blockchain->isChainValid()) {
        SuccessDialog *s = new SuccessDialog(p,"Blockchain is Valid!");
        s->show();
    }
    else {
        ErrorDialog *e = new ErrorDialog(p,"Blockchain is not valid!");
        e->show();
    }
}
