TEMPLATE = app
TARGET = coreclient

QT = core gui

greaterThan(QT_MAJOR_VERSION, 4):
    QT += widgets testlib

SOURCES += \
    main.cpp \
    block.cpp \
    blockchain.cpp \
    dbwrapper.cpp \
    ecdsa.cpp \
    gettime.cpp \
    hash.cpp \
    jsonconverter.cpp \
    merkleroot.cpp \
    wallet.cpp \
    transaction.cpp \
    transactionpool.cpp \
    signature.cpp \
    client.cpp \
    qtesla.cpp \
    tests.cpp

LIBS += \
    -ljsoncpp \
    -lcryptopp \
    -lleveldb \
    /usr/local/lib/liboqs.a \
    -lboost_system \
    -lboost_filesystem

HEADERS += \
    key.h \
    gettime.h \
    hash.h \
    merkleroot.h \
    ecdsa.h \
    signature.h \
    transaction.h \
    block.h \
    jsonconverter.h \
    dbwrapper.h \
    wallet.h \
    transactionpool.h \
    blockchain.h \
    client.h \
    qtesla.h \
    test.h
